# VERIF 4 X2HAL : vérifiez et corrigez votre fichier .bib avant import dans x2hal
Première version de ce fichier : 17/06/2024
Dernière maj de ce fichier : 17/06/2024

## En bref

Ce script est un petit programme d'aide au diagnostic lorsque x2hal (x2hal.inria.fr) "refuse" d'importer un fichier bib. Il est mis à disposition par Inria en version "alpha". En effet, tous les types et sous-types de documents de HAL ne sont pas encore pris en compte par ce script (pour la partie vérification des champs obligatoires https://doc.hal.science/x2hal/).

Ce script est un "carnet jupyter" (Jupyter Notebook) python qui permet de :
* vérifier qu'un fichier bibteX (extension .bib) ne contient pas d'erreurs dans sa structure (pas de virgules ou de ponctuation manquantes) et de faire un rapport d'erreurs
* de créer une version corrigée du fichier (sans effacer l'original)
* de vérifier la présence des champs obligatoires pour l'import dans HAL (pour les types : article, communications dans un congrès, proceedings, chapitres d'ouvrage, poster, preprint, ouvrage, rapport, thèse, HDR) - attention : il s'agit de champs obligatoires sur le portail inria.hal.science.
* d'ajouter les champs manquants avec les valeurs par défaut possibles pour chaque champ dans un nouveau fichier

## Description du script
* le script est contenu dans le fichier Verif4x2Hal.ipynb
* voir les commentaires à l'intérieur du script pour des explications sur ce qu'il fait (précédés de #)

## Comment améliorer le script ?
* si vous avez un compte gitlab, creez une "issue", sinon, contactez-moi directement.

## Installation

1.Installez un programme ("un environnement") permettant de lancer le script
Pour lancer un scrip python, il faut installer un environnement sur son ordinateur. Plusieurs options possibles pour cela, parmi lesquelles :
* L'option qui ne nécessite pas de droits administrateurs en principe est d'installer anaconda (https://www.anaconda.com/download), en version gratuite. 
* Visual Studio Code (https://code.visualstudio.com/download), de Microsoft, est aussi un environnement pratique
* Google colab (https://colab.research.google.com/?hl=fr) évite d'installer sur son ordinateur, mais pose des problèmes de confidentialité conduisant votre organisme de tutelle à proscrire cette solution. Renseignez-vous auprès de votre DSI.


2.Ouvrez cet environnement et ouvrir un fichier "Jupyter Notebook"
Une fois l'environnement installé, il faut repérer le répertoire par défaut du programme (c'est souvent dans C:/nomutilisateur/ pour Anaconda) pour y trouver le fichier de script que l'on souhaite éxecuter.

3.Anaconda :
Quand on ouvre "Anaconda", il faut ensuite ouvrir un "Carnet Jupyter".
Ce carnet s'ouvre dans le navigateur et affiche, sur la gauche, la liste des fichiers du répertoire par défaut. Votre fichier de script doit se trouver dans ce répertoire.

## Téléchargez le script qui se trouve ici
Ici, cliquez sur le fichier "Verif4x2Hal.ipynb" et lorsqu'il s'affiche, en haut à droite, cliquez sur le symbole "download" et vous téléchargerez un fichier dont l'extension est ".ipynb" à placer dans le répertoire par défaut de votre environnement python installé précédemment.

## Placez votre fichier .bib dans le répertoire par défaut de l'environnement python
Tout est dans le titre :-)

## Lancer le script pour vérifier votre fichier .bib
Attention, avant toute chose : dans le script même, penser à changer le nom du fichier par celui de votre fichier à analyser !
Un symbole de flèche pleine vers la droite est généralement l'endroit où il faut cliquer pour le script.

Le plus souvent, on lance chaque bloc de code séparément.

Il est préférable de les lancer dans l'ordre de haut en bas:
    1. vérification de la structure bibteX
    2. vérification de la présence des champs obligatoires
    3. création d'un fichier où les champs obligatoires sont ajoutés

## Corriger le fichier final
Le dernier bout de script ajoute les champs obligatoires et toutes les valeurs par défaut possibles. Il faut donc supprimer toutes les valeurs qui ne s'appliquent pas.
Par exemple, si le champ x-audience est ajouté, il aura ce contenu : 
'x-audience': 'Valeurs possibles : Internationale, International, Nationale, National, Non spécifiée, Not set',

À vous de le corriger pour garder la valeur qui convient.

## Revérifier

Ensuite, n'hésitez pas à lancer la vérification 1 à nouveau ou toutes les vérifications sur le fichier final, par sécurité.

